﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LuaInterface;

namespace LuaExplorer
{
    public partial class luaViewer : Form
    {
        Lua l;
        bool use_external_lua_state;

        public luaViewer()
        {
            InitializeComponent();
            use_external_lua_state = false;
            l = new Lua();
        }

        public luaViewer(Lua l)
        {
            InitializeComponent();
            use_external_lua_state = true;
            this.l = l;
        }

        private void checknode(TreeNode tn) 
        {
            
            tn.Nodes.Clear();
            string path = tn.FullPath;
            path = path.Replace(".[", "[");

            if (path.Contains(".fqn")) return;
            //object o = l[/*"_G." +*/ path];

            object[] temp = null;

            try { temp = l.DoString("return " + path); }
            catch (Exception ex)
            {
                listBox_exec_rets.Items.Clear();
                listBox_exec_rets.Items.Add(ex.Message);
                listBox_exec_rets.Items.Add(ex.Source);
            }



            if (temp != null && temp[0] != null)
            {
                listBox_exec_rets.Items.Clear();
                foreach (object tt in temp)
                {
                    if (tt.GetType() == typeof(LuaTable))
                    {
                        LuaTable t2 = (LuaTable)tt;
                        foreach (object p in t2.Keys)
                        {
                            if (p.GetType() == typeof(String))
                            {

                                tn.Nodes.Add(p.ToString());
                            }
                            else
                            {

                                tn.Nodes.Add("[" + p.ToString() + "]");

                            }
                        }
                    }
                    listBox_exec_rets.Items.Add(tt.ToString());
                }
            }



            markup(tn.Nodes);
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            checknode(e.Node);        
          
        }




        private void luaViewer_Shown(object sender, EventArgs e)
        {
            
            treeView1.PathSeparator = ".";
          
            treeView1.Sort();
        }

       

        private void markup(TreeNodeCollection col) 
        {
            foreach (TreeNode n in col)
            {
                string path2 = n.FullPath;
                path2 = path2.Replace(".[", "[");
                path2 = path2.Replace("..", ".");
                if (path2.Contains(".fqn")) continue;
                object o2 = l[path2];
                if (o2 != null)
                {
                    if (o2.GetType() == typeof(LuaTable))
                        n.BackColor = Color.Aquamarine;
                    if (o2.GetType() == typeof(LuaFunction))
                        n.BackColor = Color.Cyan;
                }
            }
        }

        

        private void button3_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileOk+=openFileDialog1_FileOk;
            openFileDialog1.ShowDialog();

           
        }
        private void openFileDialog1_FileOk(object sender, EventArgs e)
        {
            try
            {
                l.DoFile(openFileDialog1.FileName);
            }
            catch (Exception ex)
            {
                listBox_exec_rets.Items.Clear();
                listBox_exec_rets.Items.Add(ex.Message);
                listBox_exec_rets.Items.Add(ex.Source);
            }

        }

        private void luaViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!use_external_lua_state)
            l.Close();
        }

        private void comboBox_luaPutin_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) 
            {
                listBox_exec_rets.Items.Clear();

                string putin = comboBox_lua_input.Text;

                object[] ret = null;

                try { ret = l.DoString(putin); }
                catch (LuaInterface.Exceptions.LuaException ex) { 
                    listBox_exec_rets.Items.Add(ex.Message); 
                    listBox_exec_rets.Items.Add(ex.Source); 
                }

                
                if (ret!=null&&ret[0] != null)
                    foreach (object o in ret)
                    {
                        listBox_exec_rets.Items.Add(o.ToString());
                    }
                if(!comboBox_lua_input.Items.Contains(putin))
                comboBox_lua_input.Items.Add(putin);
                comboBox_lua_input.Text = "";
            }
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            checknode(e.Node);      
        }

        

    }
}
